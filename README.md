<h2>OpenGL Voxel Engine</h2>
<h3>Description</h3>
Simple 3D-Voxel-Engine in Modern OpenGL, created as a project to practice C++ and 3D-Graphic-Programming. <br />
Open source game assets (textures etc.) are credited in credits.txt files in their respective directories.
<br />

<h4>Features so far:</h4>
- Move in 3D-Space with WASD and Mouse <br />
- Texture loading <br /> 
- Basic lighting (Phong) with support for specular maps <br /> 
- Skyboxes <br />
- Chunking <br />
- Terrain generation using Perlin Noise <br />
- Dynamic Chunk loading <br />

